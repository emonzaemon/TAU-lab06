//package domain;
//
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//
//@Entity
//public class FilmActor {
//	
//	@Id
//	@GeneratedValue(strategy= GenerationType.IDENTITY)
//	private int id;
//	private int actorId;
//	private int filmId;
//
//	public int getId() {
//		return id;
//	}
//
//	public void setId(int id) {
//		this.id = id;
//	}
//
//	public int getFilmId() {
//		return filmId;
//	}
//
//	public void setFilmId(int idFilm) {
//		this.filmId = idFilm;
//	}
//
//	public int getActorId() {
//		return actorId;
//	}
//
//	public void setActorId(int actor) {
//		this.actorId = actor;
//	}
//}
